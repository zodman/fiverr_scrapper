import splinter
import sys
import time

# clean links sed -i s/\\?.*//g file.txt
# cat file.txt | sort -n 

browser = splinter.Browser("chrome")
for page in range(1, 4):
    browser.visit(sys.argv[1] + "?page=%s" % page)
    while browser.is_element_present_by_text("One Small Step"):
        pass

    if browser.is_element_not_present_by_text("One Small Step"):
        links = browser.find_by_xpath("//a[@class='gig-link-main js-gig-card-imp-data']")
        for link in links:
            res = link.find_by_xpath("./small/span")
            title = link.find_by_xpath("./h3").pop()
            reviews = res.pop().text.split(" ").pop().replace("(","").replace(")","")
            if 'k' in reviews.lower():
                reviews = reviews.replace("k+",'000')
            if not reviews.strip() == "":
                print("%s ## %s %s" % (reviews,title.text, link["href"]))
        time.sleep(2)   


